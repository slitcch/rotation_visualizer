#include "math/m_api.h"
#include "math/m_vec3.h"
#include "stereokit.h"
#include "stereokit_ui.h"
#include "randoviz.hpp"
#include "util/u_logging.h"

using namespace sk;

struct state {
  sk::pose_t window_pose = {vec3{0, 0, -0.4f},
                            quat_lookat(vec3_forward, vec3_zero)};
  xrt_vec3 vector = {};
};

void vec3_approximately_equal(xrt_vec3 &one, xrt_vec3 &two) {
  float len = m_vec3_len_sqrd(m_vec3_sub(one, two));

  if (len > 0.0001) {
    U_LOG_E("bad!");
    U_LOG_E("%f %f %f", one.x, one.y, one.z);
    U_LOG_E("%f %f %f", two.x, two.y, two.z);
  }

}

void update(void *ptr) {
  state &st = *(state *)ptr;

  sk::ui_window_begin("Hello!", st.window_pose, {0.4, 0.3});

  float step = 0.1;
  float line_width = 0.35;

  sk::ui_text("x");
  sk::ui_sameline();
  sk::ui_hslider("x", st.vector.x, -M_PI, M_PI, step, line_width);

  sk::ui_text("y");
  sk::ui_sameline();
  sk::ui_hslider("y", st.vector.y, -M_PI, M_PI, step, line_width);

  sk::ui_text("z");
  sk::ui_sameline();
  sk::ui_hslider("z", st.vector.z, -M_PI, M_PI, step, line_width);

  sk::ui_window_end();

  struct xrt_pose angle_axis = {};
  struct xrt_pose rodrigues = {};
  struct xrt_pose exp = {};

  float z = -0.3;
  float diff = 0.2;

  angle_axis.position = {-diff, 0, z};
  rodrigues.position = {0, 0, z};
  exp.position = {diff, 0, z};

  float vector_length = m_vec3_len(st.vector);
  xrt_vec3 normalized = st.vector;
  math_vec3_normalize(&normalized);

  math_quat_from_angle_vector(vector_length, &normalized,
                              &angle_axis.orientation);
  math_quat_from_rodrigues(&st.vector, &rodrigues.orientation);
  math_quat_exp(&st.vector, &exp.orientation);

  draw_hand_axis_at_pose(sk_from_xrt(angle_axis), "math_quat_from_angle_vector");
  draw_hand_axis_at_pose(sk_from_xrt(rodrigues), "math_quat_from_rodrigues");
  draw_hand_axis_at_pose(sk_from_xrt(exp), "math_quat_exp");


  xrt_vec3 test;

  math_quat_to_rodrigues(&rodrigues.orientation, &test);
  vec3_approximately_equal(test, st.vector); // broke here
  math_quat_ln(&exp.orientation, &test);
  vec3_approximately_equal(test, st.vector);
}

int main() {
  xrt_quat q1 = {1, 0, 0, 0};
  xrt_quat q21 = {0, 1, 0, 0};

  xrt_quat b;

  sk_settings_t settings = {};

  sk::sk_init(settings);

  struct state *st = new state;

  sk::sk_run_data(update, st, update, st);
}