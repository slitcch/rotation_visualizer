// Copyright 2022, Google, Inc.
// Copyright 2022, Collabora, Ltd.
// SPDX-License-Identifier: BSD-3-Clause
/*!
 * @file
 * @brief Autodiff-safe rotations for Levenberg-Marquardt kinematic optimizer.
 * Copied out of Ceres's `rotation.h` with some modifications.
 * @author Kier Mierle <kier@google.com>
 * @author Sameer Agarwal <sameeragarwal@google.com>
 * @author Moses Turner <moses@collabora.com>
 * @ingroup tracking
 */

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once
#include <algorithm>
#include <cmath>
#include <limits>
#include "assert.h"
#include "float.h"
#include "lm_defines.hpp"
#include "util/u_logging.h"
#include <iostream>


#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

namespace xrt::tracking::hand::mercury::lm {

// For debugging.
#if 0
#include <iostream>
#define assert_quat_length_1(q)                                                                                        \
	{                                                                                                              \
		const T scale = q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3];                                 \
		if (abs(scale - T(1.0)) > 0.001) {                                                                     \
			std::cout << "Length bad! " << scale << std::endl;                                             \
			assert(false);                                                                                 \
		};                                                                                                     \
	}
#else
#define assert_quat_length_1(q)
#endif


template <typename T>
inline void
QuaternionProduct(const Quat<T> &z, const Quat<T> &w, Quat<T> &zw)
{
	// Inplace product is not supported
	assert(&z != &zw);
	assert(&w != &zw);

	assert_quat_length_1(z);
	assert_quat_length_1(w);


	// clang-format off
  zw.w = z.w * w.w - z.x * w.x - z.y * w.y - z.z * w.z;
  zw.x = z.w * w.x + z.x * w.w + z.y * w.z - z.z * w.y;
  zw.y = z.w * w.y - z.x * w.z + z.y * w.w + z.z * w.x;
  zw.z = z.w * w.z + z.x * w.y - z.y * w.x + z.z * w.w;
	// clang-format on
}


template <typename T>
inline void
UnitQuaternionRotatePoint(const Quat<T> &q, const Vec3<T> &pt, Vec3<T> &result)
{
	// clang-format off
  T uv0 = q.y * pt.z - q.z * pt.y;
  T uv1 = q.z * pt.x - q.x * pt.z;
  T uv2 = q.x * pt.y - q.y * pt.x;
  uv0 += uv0;
  uv1 += uv1;
  uv2 += uv2;
  result.x = pt.x + q.w * uv0;
  result.y = pt.y + q.w * uv1;
  result.z = pt.z + q.w * uv2;
  result.x += q.y * uv2 - q.z * uv1;
  result.y += q.z * uv0 - q.x * uv2;
  result.z += q.x * uv1 - q.y * uv0;
	// clang-format on
}

template <typename T>
inline void
UnitQuaternionRotateAndScalePoint(const Quat<T> &q, const Vec3<T> &pt, const T scale, Vec3<T> &result)
{
	T uv0 = q.y * pt.z - q.z * pt.y;
	T uv1 = q.z * pt.x - q.x * pt.z;
	T uv2 = q.x * pt.y - q.y * pt.x;
	uv0 += uv0;
	uv1 += uv1;
	uv2 += uv2;
	result.x = pt.x + q.w * uv0;
	result.y = pt.y + q.w * uv1;
	result.z = pt.z + q.w * uv2;
	result.x += q.y * uv2 - q.z * uv1;
	result.y += q.z * uv0 - q.x * uv2;
	result.z += q.x * uv1 - q.y * uv0;

	result.x *= scale;
	result.y *= scale;
	result.z *= scale;
}


template <typename T>
inline void
AngleAxisToQuaternion(const Vec3<T> angle_axis, Quat<T> &result)
{
	const T &a0 = angle_axis.x;
	const T &a1 = angle_axis.y;
	const T &a2 = angle_axis.z;
	const T theta_squared = a0 * a0 + a1 * a1 + a2 * a2;

	// For points not at the origin, the full conversion is numerically stable.
	if (likely(theta_squared > T(0.0))) {
		const T theta = sqrt(theta_squared);
		const T half_theta = theta * T(0.5);
		const T k = sin(half_theta) / theta;
		result.w = cos(half_theta);
		result.x = a0 * k;
		result.y = a1 * k;
		result.z = a2 * k;
	} else {
		// At the origin, sqrt() will produce NaN in the derivative since
		// the argument is zero.  By approximating with a Taylor series,
		// and truncating at one term, the value and first derivatives will be
		// computed correctly when Jets are used.
		const T k(0.5);
		result.w = T(1.0);
		result.x = a0 * k;
		result.y = a1 * k;
		result.z = a2 * k;
	}
}



template <typename T>
inline void
CurlToQuaternion(const T &curl, Quat<T> &result)
{
	const T theta_squared = curl * curl;

	// For points not at the origin, the full conversion is numerically stable.
	if (likely(theta_squared > T(0.0))) {
		const T theta = curl;
		const T half_theta = curl * T(0.5);
		const T k = sin(half_theta) / theta;
		result.w = cos(half_theta);
		result.x = curl * k;
		result.y = T(0.0);
		result.z = T(0.0);
	} else {
		// At the origin, dividing by 0 is probably bad. By approximating with a Taylor series,
		// and truncating at one term, the value and first derivatives will be
		// computed correctly when Jets are used.
		const T k(0.5);
		result.w = T(1.0);
		result.x = curl * k;
		result.y = T(0.0);
		result.z = T(0.0);
	}
}

template <typename T>
inline void
SwingToQuaternion(const Vec2<T> swing, Quat<T> &result)
{

	const T &a0 = swing.x;
	const T &a1 = swing.y;
	const T theta_squared = a0 * a0 + a1 * a1;

	// For points not at the origin, the full conversion is numerically stable.
	if (likely(theta_squared > T(0.0))) {
		const T theta = sqrt(theta_squared);
		const T half_theta = theta * T(0.5);
		const T k = sin(half_theta) / theta;
		result.w = cos(half_theta);
		result.x = a0 * k;
		result.y = a1 * k;
		result.z = T(0);
	} else {
		// At the origin, sqrt() will produce NaN in the derivative since
		// the argument is zero.  By approximating with a Taylor series,
		// and truncating at one term, the value and first derivatives will be
		// computed correctly when Jets are used.
		const T k(0.5);
		result.w = T(1.0);
		result.x = a0 * k;
		result.y = a1 * k;
		result.z = T(0);
	}
}

template <typename T>
inline void
TwistToQuaternion(T twist, Quat<T> &result)
{

	// const T &a0 = swing.x;
	// const T &a1 = 0swing.y;
	const T a = twist;
	const T theta_squared = a * a;

	// For points not at the origin, the full conversion is numerically stable.
	if (likely(theta_squared > T(0.0))) {
		const T theta = sqrt(theta_squared);
		const T half_theta = theta * T(0.5);
		const T k = sin(half_theta) / theta;
		result.w = cos(half_theta);
		result.x = T(0);
		result.y = T(0);
		result.z = a * k;
	} else {
		// At the origin, sqrt() will produce NaN in the derivative since
		// the argument is zero.  By approximating with a Taylor series,
		// and truncating at one term, the value and first derivatives will be
		// computed correctly when Jets are used.
		const T k(0.5);
		result.w = T(1.0);
		result.x = T(0);
		result.y = T(0);
		result.z = a * k;
	}
}

#if 0
template <typename T>
inline void
SwingTwistToQuaternion(const Vec2<T> swing, const T twist, Quat<T> &result)
{
	//!@todo
	// Rather than doing compound operations, we should derive it and collapse them.
	T swing_x = swing.x;
	T swing_y = swing.y;

	T theta_squared_swing = swing_x * swing_x + swing_y * swing_y;

	if (theta_squared_swing > T(0.0)) {


		T theta_swing = sqrt(theta_squared_swing);

		T half_theta_swing = theta_swing / T(2);
		T halftwist = twist / T(2);

		T sin_half_theta = sin(half_theta_swing);
		T cos_half_theta = cos(half_theta_swing);

		T sin_half_twist = sin(halftwist);
		T cos_half_twist = cos(halftwist);

		T bleh = sin_half_theta * cos_half_twist;

		T blehblah = sin_half_theta * sin_half_twist;



		result.w = cos_half_theta * cos_half_twist;

		result.x = (swing_x * bleh / theta_swing) + (swing_y * twist * blehblah / (theta_swing * twist));

		result.y = (-swing_x * twist * blehblah / (theta_swing * twist)) + (swing_y * bleh / theta_swing);

		result.z = twist * sin_half_twist * cos_half_theta / twist;
	}

#if 1
	else {
		Quat<T> swing_quat = {};
		Quat<T> twist_quat = {};

		Vec3<T> aax_twist = {};

		aax_twist.x = (T)(0);
		aax_twist.y = (T)(0);
		aax_twist.z = twist;

		SwingToQuaternion(swing, swing_quat);

		AngleAxisToQuaternion(aax_twist, twist_quat);

		QuaternionProduct(swing_quat, twist_quat, result);
	}
#else
	else {
#if 0
		T theta_swing = 0;

		T half_theta_swing = theta_swing / T(2);
		T halftwist = twist / T(2);



		T sin_half_theta = 0; // sin(0);
		T cos_half_theta = 1; // cos(0);

		T sin_half_twist = 0; // sin(0);
		T cos_half_twist = 1; // cos(0);

		T bleh = 0; // sin_half_theta * cos_half_twist;

		T blehblah = 0; // sin_half_theta * sin_half_twist;
#endif


		result.w = T(1);


		result.x = (swing_x + swing_y) * T(0.5);
		// result.x = ((swing_x * 0) / 0) + ((swing_y * twist * 0) / (0 * twist));

		result.y = (-swing_x + twist + swing_y) * T(0.5);
		// result.y = (-swing_x * twist * 0 / (0 * twist)) + (swing_y * 0 / 0);


		// sin_half_twist / twist is 0.5
		result.z = twist * T(0.5);
		// result.z = (twist * 0 * 1) / (twist);
	}
#endif
}

#else

template <typename T>
inline void
SwingTwistToQuaternion(const Vec2<T> swing, const T twist, Quat<T> &result)
{
	//!@todo
	// Rather than doing compound operations, we should derive it and collapse them.
	T swing_x = swing.x;
	T swing_y = swing.y;

	T theta_squared_swing = swing_x * swing_x + swing_y * swing_y;

	if (theta_squared_swing > T(0.0)) {
		U_LOG_E("We are doing this!");

#if 0
// This worked.
		result.w =
		    cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) * cos((T(0.5)) * sqrt(pow(twist, 2)));


		result.x = swing_x * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               cos((T(0.5)) * sqrt(pow(twist, 2))) / sqrt(pow(swing_x, 2) + pow(swing_y, 2)) +
		           swing_y * twist * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               sin((T(0.5)) * sqrt(pow(twist, 2))) /
		               (sqrt(pow(swing_x, 2) + pow(swing_y, 2)) * sqrt(pow(twist, 2)));

		result.y = -swing_x * twist * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               sin((T(0.5)) * sqrt(pow(twist, 2))) /
		               (sqrt(pow(swing_x, 2) + pow(swing_y, 2)) * sqrt(pow(twist, 2))) +
		           swing_y * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               cos((T(0.5)) * sqrt(pow(twist, 2))) / sqrt(pow(swing_x, 2) + pow(swing_y, 2));

		result.z = twist * sin((T(0.5)) * sqrt(pow(twist, 2))) *
		           cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) / sqrt(pow(twist, 2));

#elif 0
		// works
		result.w = cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) * cos((T(0.5)) * twist);


		result.x = swing_x * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) * cos((T(0.5)) * twist) /
		               sqrt(pow(swing_x, 2) + pow(swing_y, 2)) +
		           swing_y * twist * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               sin((T(0.5)) * twist) / (sqrt(pow(swing_x, 2) + pow(swing_y, 2)) * twist);

		result.y = -swing_x * twist * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) *
		               sin((T(0.5)) * twist) / (sqrt(pow(swing_x, 2) + pow(swing_y, 2)) * twist) +
		           swing_y * sin((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) * cos((T(0.5)) * twist) /
		               sqrt(pow(swing_x, 2) + pow(swing_y, 2));

		result.z =
		    twist * sin((T(0.5)) * twist) * cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) / twist;

#elif 0
		T theta_swing = sqrt(theta_squared_swing);
		result.w = cos((T(0.5)) * theta_swing) * cos((T(0.5)) * twist);


		result.x =
		    swing_x * sin((T(0.5)) * theta_swing) * cos((T(0.5)) * twist) / theta_swing +
		    swing_y * twist * sin((T(0.5)) * theta_swing) * sin((T(0.5)) * twist) / (theta_swing * twist);

		result.y =
		    -swing_x * twist * sin((T(0.5)) * theta_swing) * sin((T(0.5)) * twist) / (theta_swing * twist) +
		    swing_y * sin((T(0.5)) * theta_swing) * cos((T(0.5)) * twist) / theta_swing;

		result.z =
		    twist * sin((T(0.5)) * twist) * cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) / twist;

#elif 0
		// works
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);

		T cos_half_twist = cos(half_twist);

		result.w = cos_half_theta * cos_half_twist;


		result.x = swing_x * sin((T(0.5)) * theta) * cos_half_twist / theta +
		           swing_y * twist * sin((T(0.5)) * theta) * sin((T(0.5)) * twist) / (theta * twist);

		result.y = -swing_x * twist * sin((T(0.5)) * theta) * sin((T(0.5)) * twist) / (theta * twist) +
		           swing_y * sin((T(0.5)) * theta) * cos_half_twist / theta;

		result.z =
		    twist * sin((T(0.5)) * twist) * cos((T(0.5)) * sqrt(pow(swing_x, 2) + pow(swing_y, 2))) / twist;

#elif 0
		// works
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = sin(half_twist);

		result.w = cos_half_theta * cos_half_twist;


		result.x = swing_x * sin((T(0.5)) * theta) * cos_half_twist / theta +
		           swing_y * twist * sin((T(0.5)) * theta) * sin((T(0.5)) * twist) / (theta * twist);

		result.y = -swing_x * twist * sin((T(0.5)) * theta) * sin((T(0.5)) * twist) / (theta * twist) +
		           swing_y * sin((T(0.5)) * theta) * cos_half_twist / theta;

		result.z = twist * sin((T(0.5)) * twist) * cos_half_theta / twist;
#elif 0
		// works
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = sin(half_twist);

		result.w = cos_half_theta * cos_half_twist;

		result.x = (swing_x * sin_half_theta * cos_half_twist / theta) +
		           (swing_y * twist * sin_half_theta * sin_half_twist / (theta * twist));

		result.y = (swing_y * sin_half_theta * cos_half_twist / theta) -
		           (swing_x * twist * sin_half_theta * sin_half_twist / (theta * twist));

		result.z = (twist * sin_half_twist * cos_half_theta / twist);
#elif 0

		// works
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = sin(half_twist);

		result.w = cos_half_theta * cos_half_twist;

		T x_part_1 = (swing_x * sin_half_theta * cos_half_twist / theta);
		T x_part_2 = (swing_y * sin_half_theta * sin_half_twist / theta);

		result.x = x_part_1 + x_part_2;

		T y_part_1 = (swing_y * sin_half_theta * cos_half_twist / theta);
		T y_part_2 = (swing_x * sin_half_theta * sin_half_twist / theta);

		result.y = y_part_1 - y_part_2;

		result.z = (sin_half_twist * cos_half_theta);
#elif 1
		// works
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = sin(half_twist);

		T sin_half_theta_over_theta = sin_half_theta / theta;

		result.w = cos_half_theta * cos_half_twist;

		T x_part_1 = (swing_x * cos_half_twist * sin_half_theta_over_theta);
		T x_part_2 = (swing_y * sin_half_twist * sin_half_theta_over_theta);

		result.x = x_part_1 + x_part_2;

		T y_part_1 = (swing_y * cos_half_twist * sin_half_theta_over_theta);
		T y_part_2 = (swing_x * sin_half_twist * sin_half_theta_over_theta);

		result.y = y_part_1 - y_part_2;

		result.z = sin_half_twist * cos_half_theta;

#endif
	} 
	#if 0
	else if (theta_squared_swing > T(0.0)) {
		// Swing is non-zero but twist is zero.
		// This works but turns out to not really be interesting
		T theta = sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = T(1); // cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = T(0.5) * twist; // sin(half_twist);

		T sin_half_theta_over_theta = sin_half_theta / theta;

		result.w = cos_half_theta * cos_half_twist;

		T x_part_1 = (swing_x * cos_half_twist * sin_half_theta_over_theta);
		T x_part_2 = (swing_y * sin_half_twist * sin_half_theta_over_theta);

		result.x = x_part_1 + x_part_2;

		T y_part_1 = (swing_y * cos_half_twist * sin_half_theta_over_theta);
		T y_part_2 = (swing_x * sin_half_twist * sin_half_theta_over_theta);

		result.y = y_part_1 - y_part_2;

		result.z = sin_half_twist * cos_half_theta;
	} 
	#endif
	else {
		// Everhthing is zero
		// Swing is non-zero but twist is zero.
		// This works but turns out to not really be interesting
		// works
		T theta = T(0); // sqrt(theta_squared_swing);

		T half_theta = theta * T(0.5);

		// the "other" theta
		T half_twist = twist * T(0.5);

		T cos_half_theta = cos(half_theta);
		T cos_half_twist = cos(half_twist);

		T sin_half_theta = sin(half_theta);
		T sin_half_twist = sin(half_twist);

		T sin_half_theta_over_theta = T(0.5);

		result.w = cos_half_theta * cos_half_twist;

		T x_part_1 = (swing_x * cos_half_twist * sin_half_theta_over_theta);
		T x_part_2 = (swing_y * sin_half_twist * sin_half_theta_over_theta);

		result.x = x_part_1 + x_part_2;

		T y_part_1 = (swing_y * cos_half_twist * sin_half_theta_over_theta);
		T y_part_2 = (swing_x * sin_half_twist * sin_half_theta_over_theta);

		result.y = y_part_1 - y_part_2;

		result.z = sin_half_twist * cos_half_theta;

	// std::cout << "START" << std::endl;
	// 	std::cout << result.x << " " << result.y << " " << result.z << " " << result.w << " " << std::endl;



	// 			Quat<T> swing_quat = {};
	// 	Quat<T> twist_quat = {};

	// 	Vec3<T> aax_twist = {};

	// 	aax_twist.x = (T)(0);
	// 	aax_twist.y = (T)(0);
	// 	aax_twist.z = twist;

	// 	SwingToQuaternion(swing, swing_quat);

	// 	AngleAxisToQuaternion(aax_twist, twist_quat);

	// 	QuaternionProduct(swing_quat, twist_quat, result);
	// 	std::cout << result.x << " " << result.y << " " << result.z << " " << result.w << " " << std::endl;
	// std::cout << "END" << std::endl;

	}
}
#endif
} // namespace xrt::tracking::hand::mercury::lm
